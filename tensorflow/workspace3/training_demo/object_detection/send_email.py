""" program to send csv attachment via email """
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from tkinter.messagebox import showerror,showinfo
def send_email():

	try:
		email_user = "your email"
		target_user = "target email"

		# email_user = "source email id"
		# target_user = "destination userid"

		msg = MIMEMultipart()
		msg['From'] = email_user
		msg['To'] = target_user
		msg['Subject'] = "Database converted to CSV"

		body = "hello world"
		# attaching body to msg
		msg.attach(MIMEText(body,"plain"))

		# attachment
		filename = "data.csv"
		attachment = open(filename,'rb')

		 # adding attachment and closing
		part = MIMEBase('application','octet-stream')
		part.set_payload((attachment).read())
		encoders.encode_base64(part)

		part.add_header("Content-Disposition","attachment;filename = "+filename)

		msg.attach(part)

		text = msg.as_string()

		server = smtplib.SMTP("smtp.gmail.com",587)
		server.starttls()

		server.login(email_user,"your password")
		server.sendmail(email_user,target_user,text)
		server.quit()
		showinfo("","Mail sent successfully !")
	except Exception:
		showerror("","Problem in sending email")