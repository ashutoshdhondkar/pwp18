"""
	convert the data present in database in csv

"""
import sqlite3 as sql
import csv
from datetime import date,time,datetime,timedelta
from dateutil.parser import parse
import send_email

def convert_to_csv(datefrom,dateto):

	timestamp = (parse(dateto) - parse(datefrom))
	timestamp = timestamp.days + 1
	# print(timestamp)
	# print(datefrom,dateto)
	dates_created = []

	for x in range(timestamp):
		dat = parse(datefrom) + timedelta(days = x)
		dat = dat.date()
		dates_created.append(str(dat))

	print(dates_created)

	conn = sql.connect("PWP_details.db")
	curr = conn.cursor()
	
	f = open("data.csv",'wb')
	title = "id,image_name,brand_name,number_objects,created_date,location,pincode"
	title = title+"\n"
	title = title.encode()
	f.write(title)

	data = []
	for x in dates_created:
		
		# data.append(list(curr.execute(f"select * from pwp_stats where date(created_date) = '{x}';")))
		data.append(list(curr.execute(f"select * from pwp_stats where date(created_date) = '{x}';")))
	
	
	print(data)
	

	for i in range(len(data)):
		newdata = data[i]
		for x in newdata:
			string = ','.join([str(j) for j in x])
			string += "\n"
		
			f.write(string.encode())

	f.close()
	send_email.send_email()
	conn.close()
