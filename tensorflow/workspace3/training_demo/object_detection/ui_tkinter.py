import platform # to print os version and name if required
import tkinter as tk
from tkinter.filedialog import askopenfilename
# from tkinter import simpledialog
import cv2 # imported to capture videos and images from user
from pil import Image,ImageTk
import os # for naming the files with indices
import shutil
import matplotlib
# matplotlib.use("TkAgg")

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

# database
import sqlite3 as sql
# use object_detection_trial.py
import object_detection_trial

# use bantai_video
import bantai_video

from tkinter.messagebox import askyesno,showerror

# convert to csv
import convert_to_csv
# sending mail
import tensorflow as tf

from datetime import date,time,datetime,timedelta
from dateutil.parser import parse

"""
	list = [file1,file2,file3,....]
	ask user which file he wants to check
	and then use index number specified by user to show in webbrowser
"""

class PWP18():
	
	frame_top_bg = "#4C4B4B"
	frame_nav = "#0A79DF"
	back_color = "#A4B0BD"
	top_fg = "white"
	btn_bg = "#4C4B4B"
	btn_fg = "white"
	# frame_right = "#A4B0BD"
	user_ip = []

	def __init__(self,root,location,pincode):
		self.location = location
		self.pincode = pincode
		
		self.root = root
		self.root.title("Deep Blue - PLASTIC WASTE PROFILING")
		# press escape to close the window
		self.root.bind('<Escape>',lambda e:self.root.destroy())
		self.cap = None
		
		# screen dimensions
		self.screen_height = 1000
		self.screen_width = 700
		
		# calculating x and y values
		x = (self.root.winfo_screenwidth() - self.screen_height)//2
#         y = (self.root.winfo_screenheight() - self.screen_width)//2
		
		self.root.geometry(f"{self.screen_height}x{self.screen_width}+{x}+{0}")
		self.root.resizable(False, False)
		# frame on top
		self.frame_top = tk.Frame(self.root,bg=self.frame_top_bg,height=(self.screen_height//4))
		self.frame_top.pack(side=tk.TOP,fill=tk.X)
		
		# create new frame for vertical navbar
		self.frame_nav = tk.Frame(self.root,
								  bg=self.frame_nav,
								  width = self.screen_width//4
								  )
		self.frame_nav.pack(side = tk.LEFT,fill=tk.Y)
		self.frame_nav.propagate(0)
		
		# creating frame on right
		self.frame_right = tk.Frame(self.root,
									bg=self.back_color,
									width = 100*(self.screen_width//4),
									relief = "ridge",
									bd="5",

									)
		self.frame_right.pack(side = tk.LEFT,fill=tk.BOTH)
		self.frame_right.propagate(0)

		
		# put pwp 18 on frame_top
		tk.Label(self.frame_top,
				text="Plastic Waste Profiling\n( Object Detection )",
				bg=self.frame_top_bg,
				padx = 20,
				pady = 20,
				fg = self.top_fg,
				font=('Comic Sans MS',20,'bold'),
				).pack(side = tk.LEFT)
		
		#put working os name and version
		tk.Label(self.frame_top,
				text=f"{platform.machine()}{platform.platform()}\n{platform.processor()}" ,
				bg=self.frame_top_bg,
				padx = 50,
				pady = 20,
				fg = self.top_fg,
				font=('Comic Sans MS',15)
				).pack(side = tk.RIGHT)
		
		
		# add buttons to the navbar
		self.image_btn = tk.Button(self.frame_nav,text="Image",
								   relief = 'ridge',
								   pady = 50,
								   bd=5,
								   font = ('Comic Sans MS',20,'bold'),
								   command = self.load_image,
								   bg = self.btn_bg,
								   fg = self.btn_fg
								   )
		self.image_btn.pack(fill = tk.BOTH)
		
		
		self.video_btn = tk.Button(self.frame_nav,text="Video",
								   relief = 'ridge',
								   pady = 50,
								   bd=5,
								   font = ('Comic Sans MS',20,'bold'),
								   command = self.load_video,
								   bg = self.btn_bg,
								   fg = self.btn_fg
								   )
		self.video_btn.pack(fill = tk.BOTH)
		
		self.stats_btn = tk.Button(self.frame_nav,text="Analysis",
								   relief = 'ridge',
								   pady = 40,
								   bd=5,
								   font = ('Comic Sans MS',20,'bold'),
								   command = self.load_stats,
								   bg = self.btn_bg,
								   fg = self.btn_fg
								   )
		self.stats_btn.pack(fill = tk.BOTH)
		
		self.admin_btn = tk.Button(self.frame_nav,text="Start\nProcessing",
								   relief = 'ridge',
								   pady = 50,
								   bd=5,
								   font = ('Comic Sans MS',20,'bold'),
								   command = self.admin_auth,
								   bg = self.btn_bg,
								   fg = self.btn_fg
								   )
		self.admin_btn.pack(fill = tk.BOTH)
		
		# add project description on initial screen
		tk.Label(self.frame_right,
				text=" Project Description ".center(100,'-'),
				bg=self.back_color,
				font = ('Bookman Old style',20,'bold italic')).pack(pady = 10)
		
		self.definition = """The scope and intention of this project is to implement Plastic Waste Profiling using machine learning algorithms. Enivronment used is Colaboratory
---insert technology stuff here---\n

As we know, the problem of rampant waste disposal is a big environmental and humanitarian hazard, with plastic being the biggest contributor to human waste in recent decades. Despite the ban of plastic bags, small plastic items like bottles and potato wafer packets continue to clog drains and spoil hygienic conditions. Without proper education of where to dump and how to take care of waste, there is an oncoming crisis. Plastic has already entered into the natural ecosystem. Scientific studies have shown that plastic has contiminated the food chain, the chemicals being consumed by both domestic and wild animals. Some endangered species like the Olive Ridley turtle are vanishing because they had been found with toxic plastic in their bodies. Clogged drains and dumpyards are not only ugly to look at, but they possess a safety hazard. Clogged storm drains often lead to flooding in the city, and overflowing dumpyards are a source of toxic air pollution in the case of them being burnt in a fire
It is of utmost importance that the proliferation of plastic items must be controlled, otherwise the future of our planet's ecosystem is in grave danger.
\n
The project intends to communicate to the big brands which package their items in plastic, to provide guidelines and warnings to people about proper disposal, in a way similar to how the anti-smoking warnings on cigarretes discourage potential smokers, so people know where to put their waste.
\n
"""
		
		self.text = tk.Text(self.frame_right,bd=0,bg=self.back_color,font=('Comic Sans MS',20))
		self.text.insert(tk.INSERT, self.definition)
		self.text.pack(padx = 10,fill=tk.BOTH)
		
		# disable editing in the text box
		self.text.bind('<Button-1>',lambda e:"break")
	   
						   
		# giving notification after uploading an image
		
#         self.notify = tk.Label(self.frame_right,font = ('Normal',20,'bold'))
#         self.notify.pack()
		


	def clear_frame(self,frame):
		" clears all the widgets from the specified frame "
		for widget in frame.winfo_children():
			widget.destroy()
	
	def load_image(self):
		# self.location,self.pincode = getlocation.getPosition()
		# print(f"Tkinter k andar : {self.location},{self.pincode}")
		" loads all the widgets related to camera and images "
		
		self.clear_frame(self.frame_right)
		
		if(self.cap):
			self.cap.release()
		self.cap = cv2.VideoCapture(0)
		
		"Take path for dir"
		def capture():
			num = len(os.listdir("C:\\pwp18\\tensorflow\\workspace3\\training_demo\\test"))
			tested_len = len(os.listdir("C:\\pwp18\\tensorflow\\workspace3\\training_demo\\tested"))
			cv2.imwrite("C:\\pwp18\\tensorflow\\workspace3\\training_demo\\test\\test_"+str(num)+"tested_"+str(tested_len)+".jpg", self.frame)
			self.clear_frame(self.frame_right)
			tk.Label(self.frame_right,text="Thank you\nfor keeping your\n city clean !",
					 font=('Bookman Old Style',30,'bold italic'),
					 bg=self.back_color
					 ).pack(side = tk.TOP,pady=10)
			
		def store_into_file():
			"loading images from pc and copying them into given dir"
			im = askopenfilename()
			if(im == None):
				return
			tested_len = len(os.listdir("C:\\pwp18\\tensorflow\\workspace3\\training_demo\\tested"))
			num = len(os.listdir("C:\\pwp18\\tensorflow\\workspace3\\training_demo\\test"))
			newfilename = "test_"+str(num)+"tested_"+str(tested_len)+".jpg"
			print(newfilename)
			shutil.copy(im,"C:\\pwp18\\tensorflow\\workspace3\\training_demo\\test\\" + newfilename)
			self.clear_frame(self.frame_right)
			
			# creating a notification frame
			tk.Label(self.frame_right,text="Thank you\nfor keeping your\n city clean !",
					 font=('Bookman Old Style',30,'bold italic'),
					 bg=self.back_color
					 ).pack(side = tk.TOP,pady=10)
			
			self.cap.release()
			cv2.destroyAllWindows()
			
		
			
		self.lbl = tk.Label(self.frame_right,height = 500,width = self.screen_width,bg=self.back_color,pady=5)
		self.lbl.pack(fill = tk.X,pady=2)
		
		self.capture = tk.Button(self.frame_right,text="Capture",bd=5,relief="ridge",
								 font=('Comic Sans MS',20,'bold'),
								 command = capture)
		self.capture.pack(side = tk.LEFT,padx = (self.screen_width//5 + 30))
		
		self.load_from_gallery = tk.Button(self.frame_right,text="Load From Gallery",bd=5,
										   relief="ridge",
										   font=('Comic Sans MS',20,'bold'),
										   command = store_into_file
										   )
		self.load_from_gallery.pack(side = tk.LEFT)
		
		def show_frame():
			_, self.frame = self.cap.read()
			self.frame = cv2.flip(self.frame, 1)
			self.cv2image = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGBA)
			self.img = Image.fromarray(self.cv2image)
			self.imgtk = ImageTk.PhotoImage(image=self.img)
			self.lbl.imgtk = self.imgtk
			self.lbl.configure(image = self.imgtk)
			self.lbl.after(10, show_frame)
			
		show_frame()

	
	def load_video(self):
		" every thing related to video capturing and storing "
		
		if(self.cap):
			self.cap.release()
		self.clear_frame(self.frame_right)
#         cv2.destroyAllWindows()
#         self.cap.release()
#         print(self.cap.isOpened()) # indicates that camera is closed
		self.cap = cv2.VideoCapture(0)
		
		self.flag = 0
		self.out = None
		def Start_Video():
			" set self.out "
			filename = "Video_"+str(len(os.listdir("C:\\Users\\Ashutosh\\My Documents\\LiClipse Workspace\\PWP18_ui\\videos")))
			self.out = "C:\\Users\\Ashutosh\\My Documents\\LiClipse Workspace\\PWP18_ui\\videos\\" + filename+".mp4"
			self.out = cv2.VideoWriter(self.out,
								  -1,
								  20.0,
								  (640,480)
								  )
		self.lbl = tk.Label(self.frame_right,height = 500,width = self.screen_width,bg=self.back_color,pady=5)
		self.lbl.pack(fill = tk.X)
		
		
		def show_frame():
			_, self.frame = self.cap.read()
			if(self.out):
				self.out.write(self.frame)
			self.frame = cv2.flip(self.frame, 1)
			self.cv2image = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGBA)
			self.img = Image.fromarray(self.cv2image)
			self.imgtk = ImageTk.PhotoImage(image=self.img)
			self.lbl.imgtk = self.imgtk
			self.lbl.configure(image = self.imgtk)
			
			if(self.flag == 0):
				self.lbl.after(10, show_frame)
			elif(self.flag == 1):
				self.out.release()   
				self.cap.release() # newly added         
			
		show_frame()
		
		def Stop_Video():
			"stop the recording and release all the items "
			
			if(self.cap and self.out):
				self.flag = 1
			else:
				print("Camera not opened")
		
		self.video_start_btn = tk.Button(self.frame_right,
											text = "Start",
											
											padx=20,
											bd=5,
											font=('Comic Sans MS',15,'bold'),
											command = Start_Video,
										)
		
		self.video_start_btn.pack(side=tk.LEFT,padx=100)
		
		self.video_stop_btn = tk.Button(self.frame_right,
										text = "upload",
										
										bd=5,
										padx=20,
										font=('Comic Sans MS',15,'bold'),
										
										command = show_frame
										)
		
		self.video_stop_btn.pack(side=tk.LEFT,padx = 20)
		
		
		def load_from_gallery():
			"loading videos from pc and copying them into given dir"

			im = askopenfilename()
			newim = im
			extension = newim.split('.')[-1]
			
			if(im == None):
				return
			
			newfilename = "video_"+str(len(os.listdir("C:\\Users\\Ashutosh\\My Documents\\LiClipse Workspace\\PWP18_ui\\videos")))+"."+extension
			newfilename = "C:\\pwp18\\tensorflow\\workspace3\\training_demo\\video\\demo.mp4"
			print(newfilename)
			shutil.copy(im,newfilename)
			self.clear_frame(self.frame_right)
			
			# creating a notification frame
			tk.Label(self.frame_right,text="Thank you\nfor keeping your\n city clean !",
					 font=('Bookman Old Style',30,'bold italic'),
					 bg=self.back_color
					 ).pack(side = tk.TOP,pady=10)
			
			self.cap.release()
			cv2.destroyAllWindows()
		
		self.video_load_btn = tk.Button(self.frame_right,
										text = "load from gallery",
										padx=20,
										bd=5,
										font=('Comic Sans MS',15,'bold'),
										command = load_from_gallery
										)
		
		self.video_load_btn.pack(side=tk.LEFT,padx=60)
		
		

	def load_stats(self):
		self.clear_frame(self.frame_right)

		tk.Label(self.frame_right,
			text="""Welcome to Processing section\nYou can now perform analysis based on dates.\nIf you don't mention any dates then the\nentire graph is shown
					""",bg=self.back_color,padx = 30,
			font = ('Comic Sans MS',25,'bold')

			).pack()

		self.datefrom = tk.StringVar()
		self.dateto = tk.StringVar()
		

		tk.Label(self.frame_right,text="From (yyyy-mm-dd)",bg=self.back_color,font=('Comic Sans MS',20,'bold')).pack()
		tk.Entry(self.frame_right,font=('Comic Sans MS',20,'bold italic'),textvariable = self.datefrom).pack()

		tk.Label(self.frame_right,text="To (yyyy-mm-dd)",bg=self.back_color,font=('Comic Sans MS',20,'bold')).pack()
		tk.Entry(self.frame_right,font=('Comic Sans MS',20,'bold italic'),textvariable = self.dateto).pack(pady=10)


		db = sql.connect("PWP_details.db")
		curr = db.cursor()
		brands = ['Kurkure','Lays','Sprite','Vimal','Thums Up']

		
		def bargraph(location = None):
			print(location)
			if(location):
				
				count_no = [
							list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'kurkure' and location = '{location}';"))[0][0],
							list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'lays' and location = '{location}';"))[0][0],
							list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'sprite' and location = '{location}';"))[0][0],
							list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'vimal' and location = '{location}';"))[0][0],
							list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'Thums Up' and location = '{location}';"))[0][0]
					]
			else:

				count_no = [
							list(curr.execute("select sum(number_objects) from pwp_stats where brand_name = 'kurkure'"))[0][0],
							list(curr.execute("select sum(number_objects) from pwp_stats where brand_name = 'lays'"))[0][0],
							list(curr.execute("select sum(number_objects) from pwp_stats where brand_name = 'sprite'"))[0][0],
							list(curr.execute("select sum(number_objects) from pwp_stats where brand_name = 'vimal'"))[0][0],
							list(curr.execute("select sum(number_objects) from pwp_stats where brand_name = 'Thums Up'"))[0][0]
					]
			try:

				self.timestamp = (parse(self.dateto.get()) - parse(self.datefrom.get()))
				self.timestamp = self.timestamp.days + 1

				self.dates_created = []
				for x in range(self.timestamp):
					self.dat = parse(self.datefrom.get()) + timedelta(days = x)
					self.dat = self.dat.date()
					self.dates_created.append(str(self.dat))

				print(self.dates_created)

				kurkure,lays,sprite,vimal,thumbs_up = (0,0,0,0,0)
				if(location!=None):
					for x in self.dates_created:
						kurkure += sum([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'kurkure' and date(created_date) = '{x}' and location = '{location}';"))[0]) ])
						lays += sum([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'lays' and date(created_date) = '{x}' and location = '{location}';"))[0]) ])
						sprite += sum([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'sprite' and date(created_date) = '{x}' and location = '{location}';"))[0]) ])
						vimal += sum([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'vimal' and date(created_date) = '{x}' and location = '{location}';"))[0]) ])
						thumbs_up += sum([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'Thums Up' and date(created_date) = '{x}' and location = '{location}';"))[0]) ])
				else:
					for x in self.dates_created:
						kurkure += sum([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'kurkure' and date(created_date) = '{x}';"))[0]) ])
						lays += sum([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'lays' and date(created_date) = '{x}';"))[0]) ])
						sprite += sum([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'sprite' and date(created_date) = '{x}';"))[0]) ])
						vimal += sum([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'vimal' and date(created_date) = '{x}';"))[0]) ])
						thumbs_up += sum([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'Thums Up' and date(created_date) = '{x}';"))[0]) ])
					
				count_no = [kurkure,lays,sprite,vimal,thumbs_up]
				print(count_no)
			except Exception as e:
				print(e)

				
			self.clear_frame(self.frame_right)

			
			f = Figure(figsize=(5,5),dpi=95)
			a = f.add_subplot(111)
			a.set_xlabel("Brands ---->")
			a.set_ylabel("count of plastic waste ---->")
			a.set_title("Plastic Waste Profiling (" + (location if(location) else "All") + " )")
			
			a.bar(brands,count_no,color=['red','orange','green'])
			FigureCanvasTkAgg(f,self.frame_right).get_tk_widget().pack()
			if(self.datefrom.get() and  self.dateto.get()):
				tk.Button(self.frame_right,text="View Progress",bd=5,
					  font=('Comic Sans MS',15,'bold'),
					  command=lambda : self.load_track(self.location_entry.get()),
					  fg = self.btn_fg,
					  bg = self.btn_bg
					  ).pack(side = tk.LEFT,padx = 50)


				self.location_entry = tk.StringVar()

				locs = list(curr.execute("select location from pwp_stats group by location;"))
				# print(list(curr.execute("select location from pwp_stats group by location;")))
				spinbox = tk.Spinbox(self.frame_right,textvariable = self.location_entry ,values = [x[0] for x in map(lambda x : x if(x!=None) else "None",locs)],width = 30,bd=5,font=('Arial',10,'bold'),state="readonly")
				spinbox.pack(side = tk.LEFT,padx = 20)
				
				tk.Button(self.frame_right,text="apply",bd=5,
						  font=('Comic Sans MS',15,'bold'),
						  command=lambda : bargraph(self.location_entry.get()),
						  fg = self.btn_fg,
						  bg = self.btn_bg
						  ).pack(side = tk.LEFT,padx = 50)
			else:
				tk.Button(self.frame_right,text="Set date",bd=5,
						  font=('Comic Sans MS',15,'bold'),
						  command=self.load_stats,
						  bg = self.btn_bg,
						  fg = self.btn_fg
						  ).pack(padx = 50,pady=10)
			
			

		tk.Button(self.frame_right,text="Plot",bd=5,font=('Normal',20,'bold'),
			command = bargraph ,
			bg = self.btn_bg,
			fg = self.btn_fg,
			relief="ridge").pack(pady=30,fill=tk.X,padx = 100)
	

	def load_track(self,location = None):

		self.clear_frame(self.frame_right)
		print(location)
		
		db = sql.connect("PWP_details.db")
		curr = db.cursor()        
		
		kurkure = []
		lays = []
		sprite = []
		vimal = []
		thumbs_up = []
		if(location):
			for x in self.dates_created:
				kurkure.append([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'kurkure' and date(created_date) = '{x}' and location = '{location}';"))[0]) ])
				lays.append([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'lays' and date(created_date) = '{x}' and location = '{location}';"))[0]) ])
				sprite.append([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'sprite' and date(created_date) = '{x}' and location = '{location}';"))[0]) ])
				vimal.append([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'vimal' and date(created_date) = '{x}' and location = '{location}';"))[0]) ])
				thumbs_up.append([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'Thums Up' and date(created_date) = '{x}' and location = '{location}';"))[0]) ])

		else:
			for x in self.dates_created:
				kurkure.append([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'kurkure' and date(created_date) = '{x}';"))[0]) ])
				lays.append([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'lays' and date(created_date) = '{x}';"))[0]) ])
				sprite.append([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'sprite' and date(created_date) = '{x}';"))[0]) ])
				vimal.append([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'vimal' and date(created_date) = '{x}';"))[0]) ])
				thumbs_up.append([k for k in map(lambda k: 0 if(k==None) else k , list(curr.execute(f"select sum(number_objects) from pwp_stats where brand_name = 'Thums Up' and date(created_date) = '{x}';"))[0]) ])


		kurkure = [x[0] for x in kurkure]
		lays = [x[0] for x in lays]
		sprite = [x[0] for x in sprite]
		vimal = [x[0] for x in vimal]
		thumbs_up = [x[0] for x in thumbs_up]
		
		f = Figure(figsize=(5,5),dpi=100)
		a = f.add_subplot(111)
		a.set_ylabel("Increase in plastic waste ---->")
		a.set_xlabel("Time ---->")
		a.plot(kurkure,label="Kurkure")
		a.plot(sprite,label="Sprite")
		a.plot(lays,label="Lays")
		a.plot(vimal,label="Vimal")
		a.plot(thumbs_up,label="Thums Up")
		
		a.grid(color='black', linestyle='dotted', linewidth=0.5)
		a.legend()
		FigureCanvasTkAgg(f,self.frame_right).get_tk_widget().pack()
		a.set_title(f"Tracking ( {location if(location) else ''} )...")
	
		# tk.Button(self.frame_right,text="Change date",bd=5,
		# 		  font=('Comic Sans MS',20,'bold'),
		# 		  command= self.load_stats
		# 		  ).pack(pady=10)

		locs = list(curr.execute("select location from pwp_stats group by location;"))
			# print(list(curr.execute("select location from pwp_stats group by location;")))
		spinbox = tk.Spinbox(self.frame_right,textvariable = self.location_entry ,values = [x[0] for x in map(lambda x : x if(x!=None) else "None",locs)],width = 30,bd=5,font=('Arial',10,'bold'),state="readonly")
		spinbox.pack(padx = 20)


		db.close()
		if(self.datefrom.get() and  self.dateto.get()):
				tk.Button(self.frame_right,text="Track by location",bd=5,
					  font=('Comic Sans MS',15,'bold'),
					  command=lambda : self.load_track(self.location_entry.get()),
					  bg=self.btn_bg,
					  fg = self.btn_fg
					  ).pack()
		

		

	def admin_auth(self):
		" detecting the images in test folder "
		self.clear_frame(self.frame_right)
		tk.Label(self.frame_right,
			text=""" Welcome to Processing section""",bg=self.back_color,padx = 30,
			font = ('Comic Sans MS',30,'bold')

			).pack(pady = 20)

		self.datefrom = tk.StringVar()
		self.dateto = tk.StringVar()

		tk.Label(self.frame_right,text="From (yyyy-mm-dd)",bg=self.back_color,font=('Comic Sans MS',20,'bold')).pack(pady=10)
		tk.Entry(self.frame_right,font=('Comic Sans MS',20,'bold italic'),textvariable = self.datefrom).pack()

		tk.Label(self.frame_right,text="To (yyyy-mm-dd)",bg=self.back_color,font=('Comic Sans MS',20,'bold')).pack(pady=10)
		tk.Entry(self.frame_right,font=('Comic Sans MS',20,'bold italic'),textvariable = self.dateto).pack()
		
		def mail_stuffs():
			convert_to_csv.convert_to_csv(self.datefrom.get(),self.dateto.get())
			

		tk.Button(self.frame_right,text="Email",font=('Comic Sans MS',20,'bold'),
				command = mail_stuffs,
				bg = self.btn_bg,
				fg = self.btn_fg,
			).pack(pady=20)

		tk.Button(self.frame_right,text="Start Image Detection",font=('Comic Sans MS',20,'bold'),
				command = lambda : object_detection_trial.main_function(self.location,self.pincode),
				bg = self.btn_bg,
				fg = self.btn_fg,
				).pack(pady=5,fill=tk.X,padx = 250)

		tk.Button(self.frame_right,text="Start Video Detection",font=('Comic Sans MS',20,'bold'),
				command = lambda : bantai_video.main_function(self.location,self.pincode),
				bg = self.btn_bg,
				fg = self.btn_fg,
				).pack(pady=5,fill=tk.X,padx = 200)

		# 	send_email.send_email()
def main_func(location,pincode):
	root = tk.Tk()
	obj = PWP18(root,location,pincode)
	root.mainloop()

main_func(1,2)