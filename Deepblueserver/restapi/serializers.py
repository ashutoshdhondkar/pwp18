from rest_framework import serializers
from .models import LatitudeLongitude

class LatLongSerializer(serializers.ModelSerializer):
    " show info relevant to model and perform CRUD "
    
    class Meta:
        model = LatitudeLongitude
        fields = ('id','latitude','longitude')