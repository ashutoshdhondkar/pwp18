from django.urls import path,include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('latlong',views.LatLongView)

urlpatterns = [
        path('',include(router.urls)),
        path('permission-for-position/',views.index,name="index"),
        path('getlatlong/',views.getlatlong,name="getlatlong")
    ]