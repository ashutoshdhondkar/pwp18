from django.db import models

# Create your models here.
class LatitudeLongitude(models.Model):
    latitude = models.CharField(max_length = 50)
    
    longitude = models.CharField(max_length = 50)
    
    def __str__(self):
        return str(self.id)