from django.shortcuts import render
from rest_framework import viewsets
from .models import LatitudeLongitude
from .serializers import LatLongSerializer
from django.http import JsonResponse
# Create your views here.

class LatLongView(viewsets.ModelViewSet):
    queryset = LatitudeLongitude.objects.all()
    serializer_class = LatLongSerializer

def index(request):
    return render(request,"restapi/index.html")

def getlatlong(request):
    print("hello")
    lat = request.GET.get('lat')
    long = request.GET.get('long')
    print(lat,long)
    obj = LatitudeLongitude(latitude = lat,longitude = long)
    obj.save()
    return JsonResponse("data",safe = False)